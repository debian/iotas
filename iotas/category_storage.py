from abc import ABC, abstractmethod

from iotas.category import Category


class CategoryStorage(ABC):
    """Category storage interface."""

    @abstractmethod
    def get_all_categories(self) -> list[Category]:
        """Fetch all categories from the database.

        :return: List of categories
        :rtype: list[Category]
        """
        raise NotImplementedError()
